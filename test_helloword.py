print ("Hello World's!")
print (1 + 2)

one = 1
two = 2

three = one + two

print (three)

mylist = []
mylist.append(1)
mylist.append(2)
mylist.append(3)
print(mylist[0]) # prints 1
print(mylist[1]) # prints 2
print(mylist[2]) # prints 3

# prints out 1,2,3
for x in mylist:
    print (x)
print (1 + 2.5)

print (1 + 2 * 3 / 4.0)
print (9 / 4.0)

lotsofhellos = "hello" * 10

print (lotsofhellos)

print ("%d %d %d" % (one,two,three))