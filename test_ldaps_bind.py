# Import LDAP libraries
import ldap
import ldap.modlist as modlist

# Global values
baseDN = "ou=ANUTA,ou=Role,ou=Groups,ou=NMC,ou=SERVICES,dc=stg,dc=id,dc=ubc,dc=ca"
searchScope = ldap.SCOPE_SUBTREE
ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT,ldap.OPT_X_TLS_NEVER)
name = "NMC-Anuta-Test-MP2"

# Bind to ELDAP Staging servers
try:
	l = ldap.initialize("ldaps://eldapcons.stg.id.ubc.ca:636/")
	l.protocol_version = ldap.VERSION3
	l.simple_bind_s("uid=nmc-svcanuta1,ou=Service Accounts,ou=NMC,ou=SERVICES,dc=stg,dc=id,dc=ubc,dc=ca","tj4yeTNCGJ0dDt6X2Iah")
except ldap.LDAPError, e:
	print "error. Auth Failed."
	print e

try:
	# Add an object to Anutta OU
	dn = "cn=" + name + "," + baseDN
	attrs = {}
	attrs['objectclass'] = ['top','groupOfNames','nestedGroup']
	attrs['cn'] = name
	attrs['description'] = "Test by Michael Pal"

	ldif = modlist.addModlist(attrs)

	l.add_s(dn,ldif)

	l.unbind_s()
except ldap.LDAPError, e:
	print "error. Add Entry"
	print e

ra = None
searchFilter = "cn=" + name

try:
	ldap_result_id = l.search(baseDN, searchScope, searchFilter, ra)
	result_set = []
	while 1:
		result_type, result_data = l.result (ldap_result_id, 0)
		if (result_data == []):
			break
		else:
			if result_type ==ldap.RES_SEARCH_ENTRY:
				result_set.append(result_data)
	print result_set
except ldap.LDAPError, e:
	print "error 2"
	print e

l.unbind_s()