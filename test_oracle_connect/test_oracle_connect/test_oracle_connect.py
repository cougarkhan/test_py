import cx_Oracle as cx
import os
import sys
import requests as r
import ldap3 as l

# Variables
network_db_uri = "sirius.dbs.it.ubc.ca"
network_db_port = 1521
network_db_sid = "devl11b"
network_db_dsn = cx.makedsn(network_db_uri,network_db_port,network_db_sid)
conn_username = "mpal"
conn_password = "M1ch43lP"

eldap_base_dn = "ou=ANUTA,ou=Role,ou=Groups,ou=NMC,ou=SERVICES,dc=stg,dc=id,dc=ubc,dc=ca"
eldap_search_scope = l.SEARCH_SCOPE_WHOLE_SUBTREE


# Connect to Oracle Database
try:
    network_db_con = cx.connect(conn_username, conn_password, network_db_dsn)
    print network_db_con.dsn
except:
    print "Unexpected error during connection:", sys.exc_info()[0]
    raise


# Create Cursor for DB Operations
cursor = network_db_con.cursor()
# Execute Query
results = cursor.execute("SELECT * FROM NC_ROLE_VLAN_TBL")
# Print Results
for row in results:
    #print "%s | %s" % (row[0],row[1])
    for x in xrange(0,(len(row) - 1)):
        print row[x]







# Close connection to Oracle Database
try:
    network_db_con.close()
except:
    print "Unexpected error during closing connection:", sys.exc_info()[0]
    raise

