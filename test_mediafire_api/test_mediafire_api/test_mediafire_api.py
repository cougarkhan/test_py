from mediafire import MediaFireApi

api = MediaFireApi()
session = api.user_get_session_token(
    email='michaelppal@gmail.com',
    password='S1mpl322!',
    app_id='45857')
	
# API client does not know about the token
# until explicitly told about it:
api.session = session

response = api.user_get_info()
print(response['user_info']['display_name'])