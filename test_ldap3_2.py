from ldap3 import Server, Connection, SIMPLE, SYNC, ASYNC, SUBTREE, ALL

ldap_host = "ldaps://eldapcons.stg.id.ubc.ca"
ldap_port = 636


s = Server(ldap_host, port=ldap_port, use_ssl=True, get_info=ALL)
c = Connection(s, auto_bind=True, client_strategy=SYNC, user="uid=nmc-svcanuta1,ou=Service Accounts,ou=NMC,ou=SERVICES,dc=stg,dc=id,dc=ubc,dc=ca",password="tj4yeTNCGJ0dDt6X2Iah", authentication=SIMPLE, check_names=True)
# print s.info

# request a few objects from the LDAP server
c.search(search_base='ou=ANUTA,ou=Role,ou=Groups,ou=NMC,ou=SERVICES,dc=stg,dc=id,dc=ubc,dc=ca',
         search_filter='(cn=NMC-Anuta-Test)',
         search_scope=SUBTREE,
         #attributes=['cn','givenName'],
         attributes=['*'],
         size_limit=5)
response = c.response
result = c.result
#for r in response:
#    print r['dn'], r['attributes']  # return unicode attributes
#    print r['dn'], r['raw_attributes']

for r in response:
    print r

print result 
c.unbind()